  $(function(){
    $('input[name=cep]').on('focusout', function(){
      
      var cep = $(this).val();
      var $self = $(this);
      var $endereco = $('input[name=endereco]');
      var $estado = $('.estado');

      cep = cep.split('_').join('').split('-').join('');

      if ( cep.length < 8)
        return false;

      new PNotify({
        title: 'Buscando CEP',
        text: 'Aguarde enquanto o CEP ' + cep + ' é buscado',
        type: 'info',
        styling: 'bootstrap3'
      });

      $.ajax({
        url: 'http://correiosapi.apphb.com/cep/' + cep,
        dataType: 'jsonp',
        crossDomain: true,
        contentType: "application/json",
        statusCode: {
          /* Cep encontrado */
          200: function(data) { 
              
              var end = data.tipoDeLogradouro + ' ' + data.logradouro;
              var cidade = data.cidade + '/' + data.estado;
              $endereco.val(end);

              /* 
                Procura a cidade do WS no array de cidades cacheado. 
              */
              var cidade_ws = $.grep(cidades, function(e){ return e.value == cidade });

              if ( cidade_ws.length == 1 )
              {
                $('#cidade-search').prop('placeholder', '').val(cidade_ws[0].value);
                $('#value-cidade').val(cidade_ws[0].data);
              }

              new PNotify({
                title: 'CEP encontrado',
                type: 'success',
                styling: 'bootstrap3'
              });

          } 
          ,400: function(msg) {             
              new PNotify({
                title: 'Ocorreu um erro',
                text: 'Não foi possível buscar o endereço do CEP. Favor preencher manualmente ou tentar de novo.',
                type: 'error',
                styling: 'bootstrap3'
              });

          } 
          ,404: function(msg) { 
              new PNotify({ title: 'CEP não encontrado', text: 'Favor digitar novamente.', type: 'error', styling: 'bootstrap3' }); 
          }
        }
      });
    });
  });